<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function Add($table, $data) {
        $this->db->insert($table, $data);
        if ($this->db->affected_rows() > 0)
            return $this->db->insert_id();
        return false;
    }

    public function Edit($table, $data, $field = '', $id = '') {
        if (!empty($field)):
            $this->db->where($field, $id);
        endif;

        $this->db->update($table, $data);
        if ($this->db->affected_rows() > 0)
            return true;
        return false;
    }

    public function EDITBYID($table, $data, $id = '') {
        $this->db->where($id);
        $this->db->update($table, $data);
        if ($this->db->affected_rows() > 0)
            return true;
        return false;
    }

    public function AllEdit($table, $data) {  // Without key
        $this->db->update($table, $data);
        if ($this->db->affected_rows() > 0)
            return true;
        return false;
    }

    public function EditBatch($table, $data, $keyFiels) {
        $this->db->update_batch($table, $data, $keyFiels);
        if ($this->db->affected_rows() > 0)
            return true;
        return false;
    }

    public function Del($table, $where) {
        $this->db->where($where);
        $this->db->delete($table);
        if ($this->db->affected_rows() > 0)
            return true;
        return false;
    }

    public function getLimit($table, $start = 0, $end = 0) {
        if ($start == $end || $start < 0 || $end < 0) {
            $query = $this->db->get($table);
        } else {
            $query = $this->db->get($table, $start, $end);
        }
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function getAll($table, $where = array(), $order = FALSE, $group = FALSE) {

        $this->db->where($where);

        if ($group) :
            $this->db->group_by($group);
        endif;

        if ($order) :
            while ($key = current($order)) :
                $this->db->order_by(key($order), $key);
                next($order);
            endwhile;
        endif;

        $query = $this->db->get($table);
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function getNotInIncome($where = array()) {
        $this->db->where($where);
        return $this->db->select('*')
                        ->from('student')
//                ->join('income i','i.student_id = s.id')
                        ->where('id NOT IN (select student_id from income)', NULL, FALSE)
                        ->get()->result();
//        echo $this->db->last_query();exit;
    }

    public function getARow($table, $where) {
        $this->db->where($where);
        return $this->db->get($table)->row();
    }

    public function getAVal($table, $where, $out) {

        $res = $this->db->where($where)->get($table);
        if ($res->num_rows()) {
            return $res->row()->$out;
        }
        return FALSE;
    }

    public function query($sql) { /* EXECUTE QUERY */
        $query = $this->db->query($sql);
        return ($query->num_rows() > 0) ? $query : false;
    }

    public function proc($sql) { /* CALL A PROCEDURE */
        $query = $this->db->query($sql);
        return ($query->conn_id->affected_rows > 0 ) ? true : false;
    }

    public function exists($table, $where) {
        $this->db->where($where);
        $query = $this->db->get($table);
        return ($query->num_rows() > 0) ? true : false;
    }

    public function NumOfRows($table, $where) {
        $query = $this->db->get_where($table, $where);
        return ($query->num_rows());
    }

    public function isURL($url) {
        $file_headers = @get_headers($url);
        return ($file_headers[0] == 'HTTP/1.1 404 not Found' || empty($file_headers) || !is_array($file_headers)) ? false : true;
    }

    public function getRecordOrderBy($table, $where, $order, $group = false) {
        if (!empty($where)):
            $this->db->where($where);
        endif;
        if ($group) {
            $this->db->group_by($group);
        }

        while ($key = current($order)) {
            $this->db->order_by(key($order), $key);
            next($order);
        }

        $query = $this->db->get($table);
        return ($query->num_rows() > 0) ? $query : false;
    }

    public function selAll($data) {

        /*
         * "select" => 'pages.*, menus.title AS menu_title',
         */
        $select = isset($data["select"]) ? $data["select"] : NULL;

        /*
         * "table" => "pages",
         */
        $table = isset($data["table"]) ? $data["table"] : NULL;

        /*
         * WHERE = "name='Joe' AND status='boss' OR status='active'";
         */

        $where = isset($data["where"]) ? $data["where"] : NULL;

        $where_in = isset($data["where_in"]) ? $data["where_in"] : NULL;

        /*
         * JOIN: "join" => array("menus" => "pages.menu_id=menus.id"),
         * Options are: left, right, outer, inner, left outer, and right outer.
         * DEFAULT: left
         * FOR OTHERS: table_name#join_name
         * EXAMPLE: menus#left
         */
        $join = isset($data["join"]) ? $data["join"] : NULL;

        /*
         * "order" => array("pages.title" => "desc")
         */
        $order = isset($data["order"]) ? $data["order"] : NULL;

        /*
         *  "group" => "pages.title",
         */
        $group = isset($data["group"]) ? $data["group"] : NULL;

        /*
         * "having" => "pages.id=1"
         */
        $having = isset($data["having"]) ? $data["having"] : NULL;

        /*
         * "limit" => 5,
         * "limit" => array("start"=>0, "end"=>10)
         */
        $limit = isset($data["limit"]) ? $data["limit"] : NULL;

        /*
         * "distinct" => TRUE,
         */
        $distinct = isset($data["distinct"]) ? TRUE : FALSE;

        if (empty($select)):
            $this->db->select('*');
        else:
            $this->db->select($select, FALSE);
        endif;

        /* WHERE BY CLOUSE */
        if (!empty($where)):
            $this->db->where($where);
        endif;

        if (empty($where_in)):
            $this->db->where_in($where_in["column"], $where_in["values"]);
        endif;

        $this->db->from($table);

        /* LEFT JOIN */
        if (!empty($join)):
            while ($val = current($join)) :
                $join_tbl = key($join);
                $join_type = "left";
                $join_tbl_arr = explode("#", $join_tbl);


                if (count($join_tbl_arr) > 1):
                    $join_type = $join_tbl_arr[1];
                    $join_tbl = $join_tbl_arr[0];
                endif;

                $this->db->join($join_tbl, $val, $join_type);
                next($join);
            endwhile;
        endif;

        /* GROUP BY CLOUSE */
        if (!empty($group)):
            $this->db->group_by($group);
        endif;

        /* HAVING CLOUSE */
        if (!empty($having)):
            $this->db->having($having);
        endif;

        /* ORDER BY CHECKING */
        if (!empty($order)):
            while ($val = current($order)) :
                $this->db->order_by(key($order), $val);
                next($order);
            endwhile;
        endif;

        /* LIMIT CHECKING */
        if (is_array($limit)):
            if (!empty($limit)):
                $this->db->limit($limit['start'], $limit["end"]);
            endif;
        else:
            $this->db->limit($limit);
        endif;

        /* DISTINCT CHECKING */
        if ($distinct):
            $this->db->distinct();
        endif;

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    public function post($api_name = "", $params = array(), $type = "") {

        try {
            $this->load->driver('cache');
            $cache = $this->cache->get($api_name);
            if ($cache) {
                return $this->cache->get($api_name);
            }

            $postdata = http_build_query($params);

            $requestHeaders = array(
                'Content-type: application/x-www-form-urlencoded',
                'Accept: application/json',
                'Referer: ' . $this->url_origin($_SERVER),
                'Authorization: 123456789',
                'key: ABCD',
                sprintf('Content-Length: %d', strlen($postdata))
            );

            $context = stream_context_create(
                    array(
                        'http' => array(
                            'method' => 'POST',
                            'header' => implode("\r\n", $requestHeaders),
                            'content' => $postdata,
                            'timeout' => 50000
                        )
                    )
            );

            if ($type == "bolt") {
                $url = bolt_api_url();
            } else {
                $url = api_url() . $api_name;
            }
            $response = file_get_contents($url, false, $context);
            //print_r($response);
            if (empty($response)) {
                return false;
            } else {
                $json = json_decode($response);
                $this->cache->save($api_name, $json, NULL, 360);
                $i = 0;
                return (@$json->error) ? false : $json;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    function POST_API_CURL($api, $array) {
        $username = 'admin';
        $password = '1234';

        $url = 'http://test.com/api/Example/' . $api;

        // Alternative JSON version
        // $url = 'http://twitter.com/statuses/update.json';
        // Set up and execute the curl process

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $array);

        // Optional, delete this line if your API is open
        curl_setopt($curl_handle, CURLOPT_USERPWD, $username . ':' . $password);

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);

        $result = json_decode($buffer);
        print_r($result);
        if (isset($result->status) && $result->status == 'success') {
            echo 'User has been updated.';
        } else {
            echo 'Something has gone wrong';
        }
    }

    function url_origin($s, $use_forwarded_host = false) {
        $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on' );
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . ( ( $ssl ) ? 's' : '' );
        $port = $s['SERVER_PORT'];
        $port = ( (!$ssl && $port == '80' ) || ( $ssl && $port == '443' ) ) ? '' : ':' . $port;
        $host = ( $use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST']) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null );
        $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
        return $protocol . '://' . $host;
    }

    public function getIncome($from_date = "", $to_date = "", $ser_inst = "", $ser_pay = "") {
        if ($from_date != '' && $to_date != '') {
            $this->db->where('i.invoice_date >=', $from_date);
            $this->db->where('i.invoice_date <=', $to_date);
        }
        if ($ser_inst != '') {
            $this->db->where('ins.id', $ser_inst);
        }
        if ($ser_pay != '') {
            $this->db->where('i.payment', $ser_pay);
        }
        return $this->db->select('s.*,ins.name institute,ins.id institute_id,i.id invoice_id,i.invoice_date,i.payment,i.invoice_no,i.current_paid_fee,i.amount,i.total_current_fee,c.course_name,i.course_fee,c.join_fee,c.course_month')
                        ->from('income i')
                        ->join('student s', 's.id = i.student_id')
                        ->join('course c', 'c.id = s.course')
                        ->join('institute ins', 's.institute = ins.id')
                        ->get()->result();
    }

    public function sum_search_income($from_date = "", $to_date = "", $ser_inst = "", $ser_pay = "") {
        if ($from_date != '' && $to_date != '') {
            $this->db->where('i.invoice_date >=', $from_date);
            $this->db->where('i.invoice_date <=', $to_date);
        }
        if ($ser_inst != '') {
            $this->db->where('ins.id', $ser_inst);
        }
        if ($ser_pay != '') {
            $this->db->where('i.payment', $ser_pay);
        }
        return $this->db->select('SUM(i.current_paid_fee) sum')
                        ->from('income i')
                        ->join('student s', 's.id = i.student_id')
                        ->join('course c', 'c.id = s.course')
                        ->join('institute ins', 's.institute = ins.id')
                        ->get()->row();
    }

    public function getIncomeById($id) {
        return $this->db->select('s.*,ins.name institute,ins.id institute_id,i.id invoice_id,i.current_paid_fee,i.invoice_date,i.payment,i.invoice_no,i.amount,i.total_current_fee,c.course_name,i.course_fee,c.join_fee,c.course_month')
                        ->from('income i')
                        ->join('student s', 's.id = i.student_id')
                        ->join('course c', 'c.id = s.course')
                        ->join('institute ins', 's.institute = ins.id')
                        ->where('i.id', $id)
                        ->get()->row();
    }

    public function getAllIncomeByNo($invoice_no) {
        return $this->db->select('s.*,ins.name institute,ins.id institute_id,i.id invoice_id,i.current_paid_fee,i.invoice_date,i.payment,i.invoice_no,i.amount,i.total_current_fee,c.course_name,i.course_fee,c.join_fee,c.course_month')
                        ->from('income i')
                        ->join('student s', 's.id = i.student_id')
                        ->join('course c', 'c.id = s.course')
                        ->join('institute ins', 's.institute = ins.id')
                        ->where('i.invoice_no', $invoice_no)
                        ->get()->result();
    }

    public function getCourse($student_id) {
        return $this->db->select('s.*,c.course_name,c.course_fee,c.join_fee,c.course_month')
                        ->from('student s')
                        ->join('course c', 'c.id = s.course')
                        ->where('s.id', $student_id)
                        ->get()->row();
    }

    public function getCount($table, $where) {
        $this->db->select('count(*) count');
        $this->db->from($table);
        $this->db->where($where);
        return $this->db->get()->row();
    }

    public function sum($table, $column_name, $where = array()) {
        $this->db->select('SUM(' . $column_name . ') sum');
        $this->db->where($where);
        return $this->db->get($table)->row();
    }

    public function getCountStudent($table, $column_name, $from_date='', $to_date='', $ser_inst='') {
        if ($from_date != '' && $to_date != '') {
            $this->db->where('join_date >=', $from_date);
            $this->db->where('join_date <=', $to_date);
        }
        if ($ser_inst != '') {
            $this->db->where('institute', $ser_inst);
        }
        $this->db->select('COUNT(' . $column_name . ') count');
        return $this->db->get($table)->row();
    }

    public function sum_search_expense($from_date, $to_date, $ser_inst) {
        $this->db->select('SUM(expense_amount) sum');
        if ($from_date != '' && $to_date != '') {
            $this->db->where('expense_date >=', $from_date);
            $this->db->where('expense_date <=', $to_date);
        }
        if ($ser_inst != '') {
            $this->db->where('institute', $ser_inst);
        }
        return $this->db->get('expenses')->row();
    }

    public function sum_search($table, $from_date, $to_date, $ser_inst) {
        $this->db->select('SUM(expense_amount) sum');
        if ($from_date != '' && $to_date != '') {
            $this->db->where('expense_date >=', $from_date);
            $this->db->where('expense_date <=', $to_date);
        }
        if ($ser_inst != '') {
            $this->db->where('institute', $ser_inst);
        }
        return $this->db->get('expenses')->row();
    }

    public function sumByInstitute($where = array()) {
        $this->db->select('SUM(current_paid_fee) sum');
        $this->db->from('income i');
        $this->db->join('student s', 'i.student_id = s.id');
        $this->db->where($where);
        return $this->db->get()->row();
    }

    public function getExpense($from_date = "", $to_date = "", $ser_inst = "") {
        if ($from_date != '' && $to_date != '') {
            $this->db->where('e.expense_date >=', $from_date);
            $this->db->where('e.expense_date <=', $to_date);
        }
        if ($ser_inst != '') {
            $this->db->where('institute', $ser_inst);
        }
        return $this->db->select('e.*,ec.id category_id,ec.name category_name')
                        ->from('expenses e')
                        ->join('expenses_category ec', 'e.expense_category = ec.id')
                        ->get()->result();
    }

    public function getApplicantByHospital($id) {
        return $this->db->select('a.*,h.name_eng,h.name_ar')
                        ->from('applicants a')
                        ->join('hospital h', 'a.hospital_id = h.id')
                        ->where('a.hospital_id', $id)
                        ->get()->result();
    }
    
    public function search_result($from_date, $to_date, $ser_inst) {
        if ($from_date != '' && $to_date != '') {
            $this->db->where('m.expiry_date >=', $from_date);
            $this->db->where('m.expiry_date <=', $to_date);
        }
        if ($ser_inst != '') {
            $this->db->where('m.category', $ser_inst);
        }
        $this->db->where('r.status', '1');
        return $this->db->select('m.*,r.status')
                        ->from('medicine m')
                        ->join('request r', 'm.request_no = r.request_no')
                        ->get()->result();
    }

}
