<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        if($this->input->post('btnLogin')){
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $res = $this->common_model->getARow('users', array('email' => $email, 'password' => md5($password)));
            if($res){
                redirect('users');
            }
        }
        $this->load->view('login');
    }

}
