<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function index() {
        $data['content'] = 'userList';
        $data['users'] = $this->common_model->getAll('users',array('role !=' => 'admin'));
        $this->load->view('layouts/content',$data);
    }

}
