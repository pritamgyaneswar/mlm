<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function index() {
        if($this->input->post('btnSubmit')){
            $full_name = $this->input->post('full_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $amount = $this->input->post('amount');
            $sponsor_id = $this->input->post('sponsor_id');
            $sponsor_name = $this->input->post('sponsor_name');
            $address = $this->input->post('address');
            
            $arr_user = array(
                'full_name' => $full_name,
                'email' => $email,
                'password' => md5($password),
                'amount' => $amount,
                'sponsor_id' => $sponsor_id,
                'sponsor_name' => $sponsor_name,
                'address' => $address,
            );
            $inserted_Id = $this->common_model->Add('users',$arr_user);
//            print_r($inserted_Id);exit;
            if($inserted_Id){
                redirect('login');
            }
        }
        $this->load->view('register');
    }

}
